
[myWebsite]: www.blesssistemas.com.br

# Manual do Programador
Este manual tem como propósito servir como guia para o desenvolvedor, durante todo o processo de desenvolvimento.

#### Seções
  - [Fluxo de trabalho](#fluxo-de-trabalho)
    - [Gitflow](#gitflow)
    - [Repositórios](#repositórios)
    - [Processos do desenvolvimento](#processos-do-desenvolvimento)
        - [Feature](#feature)
        - [Bugfix](#bugfix)
        - [Hotfix](#hotfix)
    - [Como gerar versão](#como-gerar-versão)
    - [Senhas necessárias](#senhas-necessárias)
  - [Clean code](#clean-code)
  - [Sistema de versionamento](#sistema-de-versionamento)
  - [Como usar o Git](#como-usar-o-git)

# **Fluxo de trabalho**

## Gitflow
O git-flow é um conjunto de extensões para o git que provê operações de alto-nível para repositórios usando o modelo de branches do Vincent Driessen.

O fluxo de trabalho determinado nesse padrão, conta com os seguintes branches:

- **Branch master** - É a branch que contém código em nível de produção, ou seja, o código mais maduro existente na sua aplicação. Todo o código novo produzido eventualmente é juntado com a branch master, em algum momento do desenvolvimento;
  
- **Branch develop** - É a branch que contém código em nível preparatório para o próximo deploy. Ou seja, quando features são terminadas, elas são juntadas com a branch develop, testadas (em conjunto, no caso de mais de uma feature), e somente depois as atualizações da branch develop passam por mais um processo para então ser juntadas com a branch master;

- **Branches feature/*** - São branches no qual são desenvolvidos recursos novos para o projeto em questão. Essas branches tem por convenção nome começando com feature/ (exemplo: feature/new-layout) e são criadas a partir da branch develop (pois um recurso pode depender diretamente de outro recurso em algumas situações), e, ao final, são juntadas com a branch develop;
 
- **Branches hotfix/*** - São branches no qual são realizadas correções de bugs críticos encontrados em ambiente de produção, e que por isso são criadas a partir da branch master, e são juntadas diretamente com a branch master e com a branch develop (pois os próximos deploys também devem receber correções de bugs críticos, certo?). Por convenção, essas branches tem o nome começando com hotfix/ e terminando com o próximo sub-número de versão (exemplo: hotfix/2.31.1), normalmente seguindo as regras de algum padrão de versionamento, como o padrão do versionamento semântico, que abordei neste post;

- **Branches release/*** - São branches com um nível de confiança maior do que a branch develop, e que se encontram em nível de preparação para ser juntada com a branch master e com a branch develop (para caso tenha ocorrido alguma correção de bug na branch release/* em questão). Note que, nessas branches, bugs encontrados durante os testes das features que vão para produção podem ser corrigidos mais tranquilamente, antes de irem efetivamente para produção. Por convenção, essas branches tem o nome começando com release/ e terminando com o número da próxima versão do software (seguindo o exemplo do hotfix, dado acima, seria algo como release/2.32.0), normalmente seguindo as regras do versionamento semântico, como falado acima;

## Repositórios
- TODO: Explicar a estrutura dos repositórios

---
## Processos do desenvolvimento
### Feature
> Desenvolva novas funcionalidades para as versões futuras.
> Normalmente existem apenas nos repositórios dos desenvolvedores ou temporariamente na origem aguardando o merge pelo pull request.

1. **Fetch no repositório local**    
    Este processo baixa todos os commits, arquivos e referências de um repositório remoto para o repositório local. 
    - No sourcetree, clique no botão `Fetch` no canto superior esquerdo
    - Na janela aberta marque a opção `Fetch from all remotes` e clique em `OK`.

2. **Iniciar alteração no bless**    
TODO - Explicar como iniciar alteração no bless
   
3. **Checkout na branch develop**    
    Este processo faz a troca para a branch develop.
    - No sourcetree, no menu a esquerda, dentro do sub-menu `BRANCHES`, clique com o botão direito na branch develop, e selecione a opção `Checkou develop...`. Ou então clique duas vezes em cima do nome da branch develop. Em ambas as opções, ao realizar o Checkout, o nome da branch ficará em negrito.
   
4. **Pull na branch develop**    
    Este processo incorpora as mudanças de um repositório remoto dentro da branch corrente. Desta forma, as alterações realizadas por outros programadores, são sincronizadas com o repositório local.
    
    - No sourcetree, clique no botão `Pull` no canto superior esquerdo.
    - Na janela aberta, conferira se o campo `Pull from remote` está selecionado `origin`, e se os campos `Remote branch to pull` e `Pull into local branch` estão iguais.
    - Deixe marcado apenas a opção `Commit merged changes immediately` e clique em OK.

5. **Criação/Atualização da branch feature**      
    Para caso não tenha sido criado a branch para a feature, e esta seja a primeira vez que esta iniciando o desenvolvimento desta feature siga o passo 4.1 (Criar branch para feature), caso o desenvolvimento já tenha sido iniciado e a branch já tenha sido criada, os passos 4.2 e 4.3 devem ser seguidos.
    
    5.1. **Criar branch para feature**    
    Este processo cria no repositório local, uma branch específica para a feature que irá ser desenvolvida, para isto, siga os seguintes passos:
    
    - No sourcetree, faça o checkout na branch develop (Clicando duas vezes na branch).
    - Clique no botão `Git-flow` no canto superior direito.
    - Na Janela em aberto, caso a opção `Start New Feature` apareça, clique nela. Caso não apareça, clique em `Other Action...` e depois em `Start New Feature`.
    - Na tela que irá aparecer, Informe no campo `Feature Name:` o número da alteração, ex: 41001-5.
    - Deixe marcado somente a opção `Latest development branch` e clique em OK.
    
    5.2. **Checkout na branch da feature**    
    Este processo faz a troca para a branch da feature criada previamente.
    
    - No sourcetree, no menu a esquerda, dentro do sub-menu `BRANCHES`, clique com o botão direito na branch, e selecione a opção `Checkou develop...`. Ou então clique duas vezes em cima do nome da branch. Em ambas as opções, ao realizar o Checkout, o nome da branch ficará em negrito.

    5.3. **Pull na branch da feature**    
    Este processo incorpora as mudanças da branch develop do repositório remoto dentro da branch corrente, e deve ser realizado diariamente, sempre que for começar o desenvolvimento na branch. Desta forma, as alterações realizadas por outros programadores, são sincronizadas com o repositório local.
    
    - No sourcetree, clique no botão `Pull` no canto superior esquerdo.
    - Na janela aberta, conferira se o campo `Pull from remote` está selecionado `origin`, se o campo `Remote branch to pull` esta selecionado `develop`, e se o campo `Pull into local branch` deve estar com o nome da sua branch.
    - Deixe marcado apenas a opção `Commit merged changes immediately` e clique em OK.  
    
6. **Desenvolver solução**    
TODO - Descrever desenvolver solução.

    6.1. **Gerar metadata do banco**
    Apenas para features com modificações no banco de dados, siga os seguintes passos:

    - Coloque o executável ISQLExtract.exe na pasta do banco de dados onde as modificações foram feitas e execute. Caso ocorra algum problema, execute o seguinte comando: `isql -extract -echo -output "caminho...\isql_metadata_dump.sql" "caminho...\DADOS.FDB"`.
    - Copie o arquivo gerado para a pasta do repositório local `<repositorio\database>`.
    
    6.2. **Atualizar banco no servidor**
    Apenas para features com modificações no banco de dados, atualize o banco de dados do servidor com as alterações realizadas no banco local. os caminhos são:    

    - SIDI: TODO
    - FINANCE: TODO
    - INJET: TODO
    - REPRES: TODO
    - FAP: TODO
    
    6.3. **Criar documento de atualização**
    Apenas para features que necessitem ter scripts rodados antes ou após a atualização, 

7. **Fazer commit na branch**    
todo - as

 ---
### Bugfix
### Hotfix
- Os hotfixes surgem da necessidade de agir imediatamente sobre uma situação indesejada na versão de produção ativa.
- Pode ser criado a partir da tag correspondente no branch master que indica a versão em produção.

## Como gerar versão